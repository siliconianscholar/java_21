// 1. Access instance methods/variables
// 2. Show error if no main() method is present
// 3. Show error if constructor coded
// 4. What happens if we try to new HelloWorld()?
// 5. What happens if another class is in this file?

int x = 5;

void main() {
    System.out.println("Hello World!");
    showX();
    new Other().test();
}

void showX() {
    System.out.println(x);
}

class SomeOtherClass {

}
